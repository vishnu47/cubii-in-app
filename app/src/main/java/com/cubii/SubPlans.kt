package com.cubii

import android.app.Activity
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.SkuDetails
import com.cubii.databinding.RowSubPlansBinding
import com.xwray.groupie.databinding.BindableItem

class SubPlans (private val skuDetails: SkuDetails,private val launchFLow: (skuDetails: SkuDetails) -> Unit) : BindableItem<RowSubPlansBinding>() {

    override fun getLayout() = R.layout.row_sub_plans

    override fun bind(viewBinding: RowSubPlansBinding, position: Int) {
        viewBinding.apply {
            planName.text = skuDetails.title
            planName.setOnClickListener {
                launchFLow(skuDetails)
            }
        }
    }

}