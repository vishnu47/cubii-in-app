package com.cubii

import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.cubii.databinding.RowSubPlansBinding
import com.xwray.groupie.databinding.BindableItem

class MyPlans (private val purchase: Purchase) : BindableItem<RowSubPlansBinding>() {

    override fun getLayout() = R.layout.row_sub_plans

    override fun bind(viewBinding: RowSubPlansBinding, position: Int) {
        viewBinding.apply {
            planName.text = purchase.originalJson
        }
    }

}