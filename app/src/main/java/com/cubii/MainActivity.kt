package com.cubii

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.billingclient.api.*
import com.cubii.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {
    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
                for (purchase in purchases) {
                    Log.d(logtag, "Purchase okay")
                    runBlocking {
                        handlePurchase(purchase)
                    }
                }
            } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                // Handle an error caused by a user cancelling the purchase flow.
                Log.d(logtag, "Purchase cancel")
            } else if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                Log.d(logtag, "Item is already purchased")
            }
            else {
                // Handle any other error codes.
                Log.d(logtag, "Purchase error ${billingResult.responseCode}")
            }
        }

    private val purchasesResponseListener =
        PurchasesResponseListener { billingResult, purchases ->
            when(billingResult.responseCode) {
                BillingClient.BillingResponseCode.OK -> {
                    val skuList = ArrayList<String>()
                    if(purchases.size > 0)
                    {
                        binding.txtIfSubscribed.text = "Already subscribed"
                        Log.d(logtag, "User have already subscribed")

                        for(purchase in purchases) {
                            skuList.add(purchase.skus[0])
                        }

                    }
                    else {
                        binding.txtIfSubscribed.text = "Please subscribe to a plan"
                        Log.d(logtag, "User have not subscribed yet show plans")

                    }
                    runBlocking {
                        querySkuDetails(skuList)
                    }
                }
                BillingClient.BillingResponseCode.USER_CANCELED -> {
                    // Handle an error caused by a user cancelling the purchase flow.
                    Log.d(logtag, "Purchase fetch cancel")
                }
                else -> {
                    // Handle any other error codes.
                    Log.d(logtag, "Purchase fetch error")
                }
            }
        }

    private lateinit var billingClient: BillingClient

    private val logtag = "Mainactivity"

    lateinit var binding: ActivityMainBinding

    private lateinit var subsLayoutManager: LinearLayoutManager

    private var subsListAdapter: GroupAdapter<GroupieViewHolder>? = null

    private fun checkPurchases() {
        Log.d(logtag, "Checking for purchases")
        billingClient.queryPurchasesAsync(BillingClient.SkuType.SUBS, purchasesResponseListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        subsListAdapter = GroupAdapter()
        subsLayoutManager = LinearLayoutManager(this)

        binding.lifecycleOwner = this
        binding.rvPlans.apply {
            layoutManager = subsLayoutManager
            adapter = subsListAdapter

        }

        billingClient =  BillingClient.newBuilder(applicationContext)
            .setListener(purchasesUpdatedListener)
            .enablePendingPurchases()
            .build()

        makeConnection()
        setContentView(binding.root)
    }

    private fun makeConnection() {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.

                    // Check if subscription feature is supported on the client device
                    val responseCode = billingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS).responseCode
                    if(responseCode != BillingClient.BillingResponseCode.OK) {
                        Log.d(logtag, "Subscription feature is not supported on the device")
                        return
                    }

                    // Now check old purchases
                    checkPurchases()
                }
            }
            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Snackbar
                    .make(binding.txtIfSubscribed,"Connection failed, please try again",Snackbar.LENGTH_LONG)
                    .setAction(
                        "Try again"
                    ) {
                        Log.d(logtag, "Retrying to connect")
                        makeConnection()
                    }.show()
            }
        })
    }



    private suspend fun querySkuDetails(skuList: ArrayList<String> = ArrayList()) {

        if(skuList.size == 0) {
            // Have to give client product id to fetch details of products
            skuList.add("com.cubii.inapp.test")
            skuList.add("com.cubii.inappbasic.test")
        }

        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)

        // leverage querySkuDetails Kotlin extension function
        val skuDetailsResult = withContext(Dispatchers.IO) {
            billingClient.querySkuDetails(params.build())
        }
        showPlans(skuDetailsResult)
        // Process the result.
    }

    private fun showPlans(skuDetailsResult: SkuDetailsResult) {
//        subsListAdapter?.clear()
        subsListAdapter?.updateAsync(skuDetailsResult.skuDetailsList!!.toSubs())
        Log.d(logtag, skuDetailsResult.toString())
    }

    private fun List<SkuDetails>.toSubs(): List<SubPlans>{
        return this.map { detail ->
            SubPlans(detail, ::launchFlow)
        }
    }

    private fun launchFlow(skuDetails: SkuDetails) {
        val flowParams = BillingFlowParams.newBuilder()
            .setSkuDetails(skuDetails)
            .build()
        val responseCode = billingClient.launchBillingFlow(this, flowParams).responseCode
        Log.d(logtag, responseCode.toString())
    }

    private suspend fun handlePurchase(purchase: Purchase) {

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged) {
                val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                    .setPurchaseToken(purchase.purchaseToken)
                val ackPurchaseResult = withContext(Dispatchers.IO) {
                    billingClient.acknowledgePurchase(acknowledgePurchaseParams.build())
                }
                Log.d(logtag, "Purchase acknowledged $ackPurchaseResult")
            }
        }
    }

}